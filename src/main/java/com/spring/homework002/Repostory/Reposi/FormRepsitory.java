package com.spring.homework002.Repostory.Reposi;
import com.spring.homework002.Model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FormRepsitory {

    private List<Article> arrimage = new ArrayList<>();
    public List<Article> getAllArrimage() {
        return arrimage;
    }
    public Article findOne(int id){
        Article article = null;
        for (Article infrom:arrimage) {
            if (infrom.getId() == id){
                article = infrom;
            }
        }
        return article;
    }
    public void addNewInform(Article infrom){
        arrimage.add(infrom);
    }

    //delete item
    public void deleteInform(int id){
        for (Article infrom:arrimage) {
            if (infrom.getId() == id){
                arrimage.remove(infrom);
            }
        }
    }

}
