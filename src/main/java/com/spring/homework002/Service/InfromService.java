package com.spring.homework002.Service;


import com.spring.homework002.Model.Article;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public interface InfromService {

    public List<Article> getAllArrimage();
    public Article findOne(int id);
    public void addNewInform(Article infrom);
    public void deleteInform(int id);

}
