package com.spring.homework002.Service.implement;

import com.spring.homework002.Model.Article;
import com.spring.homework002.Repostory.Reposi.FormRepsitory;
import com.spring.homework002.Service.InfromService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceImp implements InfromService {

    //need it to work with resource like collection or database
    private FormRepsitory informrepsitory;

@Autowired
    public ServiceImp(FormRepsitory informrepsitory){
        this.informrepsitory = informrepsitory;
    }

    @Override
    public List<Article> getAllArrimage() {
        return informrepsitory.getAllArrimage();
    }

    @Override
    public Article findOne(int id) {
        return informrepsitory.findOne(id);
    }

    @Override
    public void addNewInform(Article infrom) {
        informrepsitory.addNewInform(infrom);
    }

    @Override
    public void deleteInform(int id) {
        try {
            informrepsitory.deleteInform(id);
        }catch (Exception e){
            System.out.println(e + "give correct id");
        }
    }

}
